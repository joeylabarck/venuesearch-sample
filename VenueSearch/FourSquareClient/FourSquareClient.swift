//
//  FourSquareClient.swift
//  VenueSearch
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

struct FourSquareClient {
    
    private init() {}
    
    static func searchVenuesNear(latitude: Double = 37.7, longitude: Double = -122.4, radius: Int, matching: String, _ completion: @escaping (Result<FSQSearchVenuesResponse>) -> Void) {
        Server.dataTask(FSQSearchVenuesRequest(latitude: latitude, longitude: longitude, radius: radius, query: matching), completion)
    }
}
