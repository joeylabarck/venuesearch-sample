//
//  FSQSearchVenuesResponse.swift
//  VenueSearch
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

struct FSQSearchVenuesResponse: Decodable {
    
    private(set) var response: _FSQSearchVenuesResponse?
}

struct _FSQSearchVenuesResponse: Decodable {
    private(set) var venues: [Venue]?
}

struct Venue: Decodable {
    private(set) var name: String?
    private(set) var categories: [Category]?
    private(set) var location: Location?
    private(set) var contact: Contact?
}

struct Category: Decodable {
    private(set) var icon: Icon?
}

struct Icon: Decodable {
    private(set) var prefix: String?
    private(set) var suffix: String?
}

struct Location: Decodable {
    private(set) var address: String?
    private(set) var distance: Float?
}

struct Contact: Decodable {
    private(set) var formattedPhone: String?
}
