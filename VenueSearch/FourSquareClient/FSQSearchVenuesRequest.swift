//
//  FSQSearchVenuesRequest.swift
//  VenueSearch
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

struct FSQSearchVenuesRequest: GETURLRequestConvertible {
    
    /*
     "The Foursquare API no longer supports requests that do not pass in a version parameter.
     */
    
    var host: String = "https://api.foursquare.com"
    var path: String = "/v2/venues/search"
    var queryParameters: [String : String]? {
        return ["ll": latlon,
                "radius": "\(radius)",
                "query": query,
                "intent": "browse",
                "oauth_token": "V3IN04VHVFKVAB1OKESONN31NGPWM0JHP4S0ODM1XMP1YCI5",
                "v": "20171209",
                "limit": "50"]
    }
    
    private let latlon: String
    private let radius: Int
    private let query: String
    
    init(latitude: Double, longitude: Double, radius: Int = 250, query: String) {
        self.latlon = "\(latitude)" + "," + "\(longitude)"
        self.radius = radius
        self.query = query
    }
}
