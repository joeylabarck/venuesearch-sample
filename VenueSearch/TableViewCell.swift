//
//  TableViewCell.swift
//  VenueSearch
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var distance: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.iconView.image = nil
        self.name.text = nil
        self.address.text = nil
        self.phoneNumber.text = nil
        self.distance.text = nil
    }
}
