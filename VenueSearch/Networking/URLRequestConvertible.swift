//
//  URLRequestConvertible.swift
//  VenueSearch
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import Foundation

protocol URLRequestConvertible {
    
    var host: String { get }
    var path: String { get}
    var headers: [String: String]? { get }
    var request: URLRequest? { get }
}

protocol POSTURLRequestConvertible: URLRequestConvertible {
    var body: Data? { get }
}

protocol GETURLRequestConvertible: URLRequestConvertible {
    var queryParameters: [String: String]? { get }
}

extension URLRequestConvertible {
    
    var headers: [String: String]? {
        return nil
    }
    
    var request: URLRequest? {
        
        guard var components = URLComponents(string: host + path) else { return nil }
        components.queryItems = (self as? GETURLRequestConvertible)?.queryParameters?.map({ URLQueryItem(name: $0, value:$1) })
        
        guard let url = components.url else { return nil }
        
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = headers
        request.httpBody = (self as? POSTURLRequestConvertible)?.body
        request.httpMethod = "GET"
        return request
    }
}
