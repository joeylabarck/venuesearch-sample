//
//  Networking.swift
//  VenueSearch
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

// TODO: Cancel Requests
import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}

enum NetworkError: Error {
    case error(Error)
    case badRequest
    case noResponse
    case noData
    case jsonDeserializationFailure
}

struct Server: _Server {}

protocol _Server {
    
    static var server: URLSession { get }
    
    static func dataTask<T: Decodable>(_ request: URLRequestConvertible, _ completion: @escaping (Result<T>) -> Void)
    
    static func downloadImageFromURL(_ url: URL, _ completion: @escaping (Result<Data>) -> Void)
}

extension _Server {
    
    static var server: URLSession {
        // TODO: TLS Public Key Pinning Object can be set here.
        return URLSession(configuration: .ephemeral, delegate: nil, delegateQueue: nil)
    }
}

extension _Server {
    
    static func dataTask<T: Decodable>(_ request: URLRequestConvertible, _ completion: @escaping (Result<T>) -> Void) {
        
        guard let request = request.request else { return }
        
        self.server.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                print("There was an error")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                print("There was no response from the server")
                return
            }
            
            guard let data = data else {
                print("There was no data returned from the server")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let object = try decoder.decode(T.self, from: data)
                completion(.success(object))
            } catch {
                completion(.failure(error))
            }
            
            }.resume()
    }
    
    static func downloadImageFromURL(_ url: URL, _ completion: @escaping (Result<Data>) -> Void) {
        
        self.server.dataTask(with: url, completionHandler: { data, response, error in
            
            guard let data = data else {
                completion(.failure(NetworkError.noData))
                return
            }
            
            completion(.success(data))
            
        }).resume()
    }
}
