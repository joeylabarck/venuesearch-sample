//
//  VenueSearchViewInteractor.swift
//  VenueSearch
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import CoreLocation
import Foundation
import UIKit

protocol VenueSearchInteractorDelegate: class {
    
    func present(_ viewController: UIViewController)
    
    func reloadResults(_ indexPaths: [IndexPath]?)
}

class VenueSearchInteractor: NSObject, UISearchBarDelegate {
    
    private weak var delegate: VenueSearchInteractorDelegate?
    
    private var venues: [Venue]? {
        didSet {
            didSetVenues()
        }
    }
    
    private lazy var locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.delegate = self
        return locationManager
    }()
    
    private var lastQuery: String? = nil
    
    private var categoryIconCache: [String: UIImage] = [:]
    
    init(delegate: VenueSearchInteractorDelegate) {
        self.delegate = delegate
    }
    
    private func didSetVenues() {
        
        setIcons()
        
        DispatchQueue.main.async {
            
            self.delegate?.reloadResults(nil)
        }
    }
    
    private func setIcons() {
        
        guard let venues = venues else { return }
        
        for (i,v) in venues.enumerated() {
            
            guard let prefix = v.categories?.first?.icon?.prefix else { return }
            guard let suffix = v.categories?.first?.icon?.suffix else { continue }
            
            let urlString = prefix + "bg_88" + suffix
            
            print(urlString)
            
            if self.categoryIconCache[urlString] == nil {
                guard let url = URL(string: urlString) else { continue }
                self.requestIcon(url, index: i)
            } else {
                
            }
        }
    }
    
    private func requestIcon(_ url: URL, index: Int) {
        
        Server.downloadImageFromURL(url, { result in
            
            switch result {
                
            case let .success(data):
                
                guard let image = UIImage(data: data) else { return }
                
                self.categoryIconCache[url.absoluteString] = image
                
                DispatchQueue.main.async {
                    self.delegate?.reloadResults([IndexPath(row: index, section: 0)])
                }
                
            case .failure: break
            }
        })
    }
    
    private func presentGenericError() {
        
        let alert = UIAlertController(title: "Something went wrong.", message: "Try again?", preferredStyle: .alert)
        let nah = UIAlertAction(title: "Nah", style: .cancel, handler: nil)
        let sure = UIAlertAction(title: "Sure", style: .default, handler: { _ in
            self.locationManager.requestLocation()
        })
        
        alert.addAction(nah)
        alert.addAction(sure)
        
        DispatchQueue.main.async {
            self.delegate?.present(alert)
        }
    }
    
    private func runSearchQuery(_ query: String, location: CLLocationCoordinate2D) {
        
        FourSquareClient.searchVenuesNear(latitude: location.latitude, longitude: location.longitude, radius: 5000, matching: query, { response in
            
            switch response {
            case let .success(object):
                
                guard let venues = object.response?.venues, venues.count > 1 else {
                    self.presentGenericError()
                    return
                }
                
                self.venues = venues
                
            case .failure:
                self.presentGenericError()
            }
        })
    }
    
    // MARK: UISearchBarDelegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        
        // TODO: Limit the amount of requests going into the server, also make requests while type-pauses could make for a bit faster experience
        self.lastQuery = searchBar.text
        
        guard CLLocationManager.locationServicesEnabled() else {
            locationManager.requestWhenInUseAuthorization()
            return
        }
        
        locationManager.requestLocation()
    }
}

extension VenueSearchInteractor: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venues?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "venueTableViewCell", for: indexPath) as? TableViewCell else { return UITableViewCell() }
        
        cell.address.text = venues?[indexPath.row].location?.address
        
        if let distance = venues?[indexPath.row].location?.distance {
            cell.distance.text = "\(distance) m"
        }
        
        if let prefix = self.venues?[indexPath.row].categories?.first?.icon?.prefix,
            let suffix = self.venues?[indexPath.row].categories?.first?.icon?.suffix,
            let image = self.categoryIconCache[prefix + "bg_88" + suffix] {
            
            cell.iconView.image = image
        }
        
        cell.name.text = self.venues?[indexPath.row].name
        
        return cell
    }
}

extension VenueSearchInteractor: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let query = self.lastQuery else { return }
        guard let location = locations.first?.coordinate else { return }
        
        self.runSearchQuery(query, location: location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        self.presentGenericError()
    }
}
