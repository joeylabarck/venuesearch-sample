//
//  ViewController.swift
//  VenueSearch
//
//  Created by Joey LaBarck on 12/8/17.
//  Copyright © 2017 Joey LaBarck. All rights reserved.
//

import UIKit

class ViewController: UIViewController, VenueSearchInteractorDelegate {

    @IBOutlet weak var searchEntry: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    private lazy var interactor: VenueSearchInteractor = {
        return VenueSearchInteractor(delegate: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Search"
        self.searchEntry.delegate = interactor
        //self.tableView.register(TableViewCell.self, forCellReuseIdentifier: "venueTableViewCell")
        //Apparently, when registering a nib based cell, you must register the nib, not the class, otherwise when it is created, the outlets are ignored...
        self.tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "venueTableViewCell")
        self.tableView.dataSource = interactor
        self.tableView.rowHeight = 90.0
    }
    
    // MARK: VenueSearchInteractorDelegate
    func present(_ viewController: UIViewController) {
        present(viewController, animated: true)
    }
    
    func reloadResults(_ indexPaths: [IndexPath]? = nil) {
        
        self.searchEntry.resignFirstResponder()
        
        guard let indexPaths = indexPaths else {
            self.tableView.reloadData()
            return
        }
        
        self.tableView.reloadRows(at: indexPaths, with: .fade)
    }
}
